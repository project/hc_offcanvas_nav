<?php

namespace Drupal\hc_offcanvas_nav\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hc_offcanvas_nav menu block.
 *
 * @Block(
 *   id = "hc_offcanvas_nav_push_menu",
 *   admin_label = @Translation("HC Off-canvas Nav")
 * )
 */
class OffCanvas extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuLinkTreeInterface $menu_tree, MenuActiveTrailInterface $menu_active_trail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTree = $menu_tree;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Load the selected menu.
    $menu_name = \Drupal::config('hc_offcanvas_nav.settings')->get('push_menu');

    \Drupal::ModuleHandler()->alter('hc_offcanvas_nav_push_menu_name', $menu_name);

    $menu_tree = \Drupal::service('menu.link_tree');
    // Build the typical default set of menu tree parameters.
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);
    $depth = \Drupal::config('hc_offcanvas_nav.settings')->get('push_depth');
    $parameters->setMaxDepth($depth);
    $parameters->expandedParents = [];

    // Load the tree based on this set of parameters.
    $tree = $menu_tree->load($menu_name, $parameters);

    // Transform the tree using the manipulators you want.
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkNodeAccess'],
    // Only show links that are accessible for the current user.
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
    // Use the default sorting of menu links.
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    $tree = $menu_tree->transform($tree, $manipulators);

    // Finally, build a renderable array from the transformed tree.
    $menu = $menu_tree->build($tree);

    \Drupal::ModuleHandler()->alter('hc_offcanvas_nav_push_tree', $menu);

    $menu['#theme'] = 'hc_offcanvas_nav_items';

    $output = [
      '#theme' => 'hc_offcanvas_nav_block_content',
      '#element_type' => \Drupal::config('hc_offcanvas_nav.settings')->get('push_wrapping_element'),
      '#content' => $menu,
    ];

    // Add a contextual link to edit the menu.
    $output['#contextual_links']['menu'] = [
      'route_parameters' => [
        'menu' => $menu_name,
      ],
    ];

    $treeBuild = $this->menuTree->build($tree);
    $output['#cache'] = $treeBuild['#cache'];
    $output['#attached']['library'][] = 'hc_offcanvas_nav/hc_offcanvas_nav_widget';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {

    // Even when the menu block renders to the empty string for a user, we want
    // the cache tag for this menu to be set: whenever the menu is changed, this
    // menu block must also be re-rendered for that user, because maybe a menu
    // link that is accessible for that user has been added.
    $cache_tags = parent::getCacheTags();
    $cache_tags[] = 'config:block.block.pushmenu';
    return $cache_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {

    // ::build() uses MenuLinkTreeInterface::getCurrentRouteMenuTreeParameters()
    // to generate menu tree parameters, and those take the active menu trail
    // into account. Therefore, we must vary the rendered menu by the active
    // trail of the rendered menu.
    // Additional cache contexts, e.g. those that determine link text or
    // accessibility of a menu, will be bubbled automatically.
    $config = \Drupal::config('hc_offcanvas_nav.settings');
    $menu_name = $config->get('push_menu');
    \Drupal::ModuleHandler()->alter('hc_offcanvas_nav_push_menu_name', $menu_name);
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'route.menu_active_trails:' . $menu_name,
    ]);
  }

}
