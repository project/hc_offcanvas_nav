<?php

namespace Drupal\hc_offcanvas_nav\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for hc_offcanvas_nav admin settings page.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hc_offcanvas_nav_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hc_offcanvas_nav.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['hc_offcanvas_nav'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('HC Off-canvas Multi-level Nav'),
    ];
    $form['hc_offcanvas_nav']['push_menu'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose Drupal menu will be displayed as a HC Off-canvas Nav'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('push_menu'),
      '#options' => $this->getCoreMenu(),
    ];

    $form['hc_offcanvas_nav']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu Position'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('position'),
      '#options' => array_combine(
        ['left', 'top', 'bottom', 'right'],
        ["Left", "Top", "Bottom", "Right"]
      ),
    ];

    $form['hc_offcanvas_nav']['push_depth'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu Depth'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('push_depth'),
      '#options' => array_combine(
        [1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 5, 6, 7, 8, 9]
      ),
    ];

    $form['hc_offcanvas_nav']['open_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Open Level Type'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('open_type'),
      '#options' => array_combine(
        ['overlap', 'expand', 'none'],
        ["OVERLAP LEVELS", "EXPEND LEVELS", "UNFOLDED LEVELS"]
      ),
    ];

    $form['hc_offcanvas_nav']['bg_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Menu Background Color'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('bg_color'),
    ];
    $form['hc_offcanvas_nav']['border_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Border Color'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('border_color'),
    ];
    $form['hc_offcanvas_nav']['color'] = [
      '#type' => 'color',
      '#title' => $this->t('Menu Text Color'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('color'),
    ];
    $form['hc_offcanvas_nav']['optional'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('ADDITIONAL OPTIONS'),
    ];
    $form['hc_offcanvas_nav']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Navbar Title'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('title'),
    ];
    $form['hc_offcanvas_nav']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Menu Width'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('width'),
    ];
    $form['hc_offcanvas_nav']['pushcontent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('If Push Content to Enter Selector like #content or .content'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('pushcontent'),
    ];
    $form['hc_offcanvas_nav']['onclick'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CLOSE ON CLICK'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('onclick'),
    ];
    $form['hc_offcanvas_nav']['levelTitles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('CLOSE Level Titles'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('levelTitles'),
    ];
    $form['hc_offcanvas_nav']['leveltitleasback'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Level Title As Back'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('leveltitleasback'),
    ];
    $form['hc_offcanvas_nav']['insertback'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert Back'),
      '#default_value' => \Drupal::config('hc_offcanvas_nav.settings')->get('insertback'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Save all the submitted form values into config.
    \Drupal::configFactory()
      ->getEditable('hc_offcanvas_nav.settings')
      ->set('push_menu', $values['push_menu'])
      ->set('position', $values['position'])
      ->set('push_depth', $values['push_depth'])
      ->set('bg_color', $values['bg_color'])
      ->set('border_color', $values['border_color'])
      ->set('color', $values['color'])
      ->set('title', $values['title'])
      ->set('width', $values['width'])
      ->set('pushcontent', $values['pushcontent'])
      ->set('open_type', $values['open_type'])
      ->set('onclick', $values['onclick'])
      ->set('levelTitles', $values['levelTitles'])
      ->set('leveltitleasback', $values['leveltitleasback'])
      ->set('insertback', $values['insertback'])
      ->save();

    // Applied when rebuilding the block on view.
    Cache::invalidateTags(['config:block.block.pushmenu']);

    parent::submitForm($form, $form_state);
  }

  /**
   * Load multiple entities, also exists as entity_load_multiple().
   */
  protected function getCoreMenu(array $core_Menu = NULL) {
    // Load the storage manager of our entity.
    $storage = \Drupal::entityTypeManager()->getStorage('menu');
    $options = [];
    // Now we can load the entities.
    foreach ($storage->loadMultiple($core_Menu) as $menus) {
      $options[$menus->id()] = $menus->label();
    }
    return $options;
  }

}
