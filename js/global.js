/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.off_canvas = {
    attach: function (context, settings) {		
	  if (!once('tset', 'html').length) {
    	return;
	  }
	
	  jQuery(window).on('load', function() {				 
		$("body").addClass('theme-default');	   
          var Nav = new hcOffcanvasNav('#main-nav', {
   			  width: drupalSettings.menu.width,
			  height: 'auto',	
			  navTitle: '',
              disableAt: false,
              customToggle: '.toggle',
              levelSpacing: 40,
		      insertBack: drupalSettings.menu.insertback,
              navTitle: drupalSettings.menu.title,
			  position:  drupalSettings.menu.position,
              levelTitles: drupalSettings.menu.levelTitles,
              levelTitleAsBack: drupalSettings.menu.leveltitleasback,
              pushContent: drupalSettings.menu.pushcontent,
			  closeOpenLevels: true,
              closeOnClick: false,
			//insertBack: true,
			  levelOpen: drupalSettings.menu.open_type,
              labelClose: ''
          });		 
		$("body.theme-default .hc-offcanvas-nav .nav-container, body.theme-default .hc-offcanvas-nav .nav-wrapper, body.theme-default .hc-offcanvas-nav ul, body.theme-default .hc-offcanvas-nav .nav-back a, body.theme-default .hc-offcanvas-nav .nav-content>.nav-close:first-child a").css({"background-color": drupalSettings.menu.bg_color});		 
		$("body.theme-default .hc-offcanvas-nav.nav-levels-expand li, body.theme-default .hc-offcanvas-nav.nav-levels-none li").css({"background-color": drupalSettings.menu.bg_color});
		$('body.theme-default .hc-offcanvas-nav .nav-content>.nav-close:first-child a, body.theme-default .hc-offcanvas-nav .nav-title+.nav-close a.has-label, body.theme-default .hc-offcanvas-nav li.nav-close a, body.theme-default .hc-offcanvas-nav .nav-back a').css({"border-top":"1px solid" + drupalSettings.menu.border_color});
		$('body.theme-default .hc-offcanvas-nav .nav-wrapper>.nav-content>ul:first-of-type>li:first-child:not(.nav-back):not(.nav-close)>.nav-item-wrapper>.nav-item-link').css({"border-top":"1px solid" + drupalSettings.menu.border_color});
		$('body.theme-default .hc-offcanvas-nav .nav-item-link, body.theme-default .hc-offcanvas-nav li.nav-close a, body.theme-default .hc-offcanvas-nav .nav-back a').css({"border-bottom":"1px solid" + drupalSettings.menu.border_color});
		$('body.theme-default .hc-offcanvas-nav .nav-wrapper>.nav-content>ul:first-of-type>li:first-child:not(.nav-back):not(.nav-close)>.nav-item-wrapper>.nav-item-link+a').css({"border-top":"1px solid" + drupalSettings.menu.border_color});
		$('body.theme-default .hc-offcanvas-nav a.nav-next').css({"border-left":"1px solid" + drupalSettings.menu.border_color, "border-bottom":"1px solid" + drupalSettings.menu.border_color});
		$('a.toggle.hc-nav-trigger.hc-nav-1').css('z-index','0');
		$("body.theme-default .hc-offcanvas-nav .nav-item-link, body.theme-default .hc-offcanvas-nav li.nav-close a, body.theme-default .hc-offcanvas-nav .nav-back a, body.theme-default .hc-offcanvas-nav .nav-content>h2").css({"color": drupalSettings.menu.color});
	  });
    }
  }
})(jQuery, Drupal, drupalSettings);
