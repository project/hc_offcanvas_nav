# HC Off-canvas Nav Module

## Table of Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Recommended Modules](#recommended-modules)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This module allows for integration of the JavaScript library for creating HC Off-canvas multi-level navigations with Drupal.

- For full Library Info, visit the [Library page](https://somewebmedia.github.io/hc-offcanvas-nav).

## Requirements

This module requires the following jQuery library for Drupal core:

- [HC Off-canvas Nav Library Releases](https://github.com/somewebmedia/hc-offcanvas-nav/releases)

## Recommended Modules

- [font-awesome module](https://www.drupal.org/project/fontawesome)
- [fontawesome_menu_icons module](https://www.drupal.org/project/fontawesome_menu_icons)

## Installation

### NPM Installation

This package can be installed with:

```bash
npm install --save hc-offcanvas-nav

## Configuration

Navigate to Administration > Extend and enable the module.
Go to Administation > Structure > System > Configure Menu Setting to
your need.
Go to Administation > Structure > Block layout > select and add a Stylsih
Multilevel
Push Menu block to a region.